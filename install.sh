#!/bin/bash

# Copy main script
sudo cp -v iuvolt /usr/bin/
sudo chmod +x /usr/bin/iuvolt

# Set voltage configuration
sudo cp -v iuvolt.cfg /etc/

# Configure msr module to load on boot
sudo mkdir /etc/modules-load.d
echo "msr" | sudo tee /etc/modules-load.d/iuvolt.conf

# Run iuvolt
sudo modprobe msr
sudo iuvolt

# Copy systemd service
sudo cp -v iuvolt.service /etc/systemd/system/
sudo systemctl enable iuvolt.service

